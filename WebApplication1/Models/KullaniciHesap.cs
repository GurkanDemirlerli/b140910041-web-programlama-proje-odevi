﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace WebApplication1.Models
{
    public class KullaniciHesap
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Ad Alanı Boş Bırakılamaz")]
        public string Adi { get; set; }
        [Required(ErrorMessage = "Soyad Alanı Boş Bırakılamaz")]
        public string Soyadi { get; set; }
        [Required(ErrorMessage = "Kullanıcı Adı Alanı Boş Bırakılamaz")]
        public string KullaniciAdi { get; set; }
        [Required(ErrorMessage = "Parola Alanı Boş Bırakılamaz")]
        [DataType(DataType.Password)]
        public string Parola { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Geçerli Bir E-Mail Adresi Girin")]
        public string Email { get; set; }
        [Compare("Parola", ErrorMessage = "Lütfen Şifrenizi Doğru Giriniz.")]
        [DataType(DataType.Password)]
        public string OnaylaParola { get; set; }

    }
}