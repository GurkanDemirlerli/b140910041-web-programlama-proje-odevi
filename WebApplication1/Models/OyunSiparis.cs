﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class OyunSiparis
    {
        public int OyunSiparisID { get; set; }
        public int OyunID { get; set; }
        public int CdKeyID { get; set; }
        public int KullaniciID { get; set; }
        public DateTime Tarih { get; set; }

        public virtual CdKey CdKey { get; set; }
        public virtual Oyun Oyun { get; set; }
    }
}