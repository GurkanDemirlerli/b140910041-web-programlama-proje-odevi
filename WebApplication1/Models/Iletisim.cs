﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Iletisim
    {
        public int ID { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Geçerli Bir E-Mail Adresi Girin")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Bu Alan Boş Bırakılamaz")]
        public string Mesaj { get; set; }
        public DateTime Tarih { get; set; }

    }
}