﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CdKey
    {
        public int CdKeyID { get; set; }
       // public int OyunID { get; set; }
        public string Key { get; set; }
        public bool Durum { get; set; }
        public DateTime Tarih { get; set; }

        public virtual Oyun Oyun { get; set; }
    }
}