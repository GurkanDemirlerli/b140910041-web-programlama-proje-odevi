﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Yorum
    {
        public int ID { get; set; }
        public int OyunID { get; set; }
        public int KullaniciID { get; set; }
        public string YorumTuru { get; set; }
        public string Icerik { get; set; }
        public DateTime Tarih { get; set; }

        public virtual Kullanici Kullanici { get; set; }
        public virtual Oyun Oyun { get; set; }
    }
}