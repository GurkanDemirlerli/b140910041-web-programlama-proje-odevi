﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.Models 
{
    public class Oyun
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public double Fiyat { get; set; }
        public string Kategori { get; set; }
        public DateTime Tarih { get; set; }
        public int Puan { get; set; }
        public string Video { get; set; }
        public string MinSistem { get; set; }
        public string OnerilenSistem { get; set; }
        public string Tanitim { get; set; }
        public string Platform { get; set; }
        public int StokDurumu { get; set; }
        public string scr1 { get; set; }
        public string scr2 { get; set; }
        public string scr3 { get; set; }
        public string scr4 { get; set; }
        public string scr5 { get; set; }
        public string scr6 { get; set; }
        public string cover { get; set; }
        public string banner { get; set; }
        public int Satilan { get; set; }

        public virtual ICollection<Yorum> Yorumlar { get; set; }
        public virtual ICollection<CdKey> CdKey { get; set; }
    }
}