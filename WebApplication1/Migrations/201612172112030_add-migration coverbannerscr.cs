namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrationcoverbannerscr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Oyun", "scr1", c => c.String());
            AddColumn("dbo.Oyun", "scr2", c => c.String());
            AddColumn("dbo.Oyun", "scr3", c => c.String());
            AddColumn("dbo.Oyun", "scr4", c => c.String());
            AddColumn("dbo.Oyun", "scr5", c => c.String());
            AddColumn("dbo.Oyun", "scr6", c => c.String());
            AddColumn("dbo.Oyun", "cover", c => c.String());
            AddColumn("dbo.Oyun", "banner", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Oyun", "banner");
            DropColumn("dbo.Oyun", "cover");
            DropColumn("dbo.Oyun", "scr6");
            DropColumn("dbo.Oyun", "scr5");
            DropColumn("dbo.Oyun", "scr4");
            DropColumn("dbo.Oyun", "scr3");
            DropColumn("dbo.Oyun", "scr2");
            DropColumn("dbo.Oyun", "scr1");
        }
    }
}
