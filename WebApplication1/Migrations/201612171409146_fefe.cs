namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fefe : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CdKey", "KullaniciID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CdKey", "KullaniciID", c => c.Int(nullable: false));
        }
    }
}
