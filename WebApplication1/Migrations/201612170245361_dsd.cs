namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dsd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CdKey",
                c => new
                    {
                        CdKeyID = c.Int(nullable: false, identity: true),
                        OyunID = c.Int(nullable: false),
                        KullaniciID = c.Int(nullable: false),
                        Key = c.String(),
                        Durum = c.Boolean(nullable: false),
                        Tarih = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CdKeyID);
            
            CreateTable(
                "dbo.OyunSiparis",
                c => new
                    {
                        OyunSiparisID = c.Int(nullable: false, identity: true),
                        OyunID = c.Int(nullable: false),
                        CdKeyID = c.Int(nullable: false),
                        KullaniciID = c.Int(nullable: false),
                        Tarih = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OyunSiparisID)
                .ForeignKey("dbo.CdKey", t => t.CdKeyID, cascadeDelete: true)
                .Index(t => t.CdKeyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OyunSiparis", "CdKeyID", "dbo.CdKey");
            DropIndex("dbo.OyunSiparis", new[] { "CdKeyID" });
            DropTable("dbo.OyunSiparis");
            DropTable("dbo.CdKey");
        }
    }
}
