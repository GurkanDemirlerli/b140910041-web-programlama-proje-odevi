namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oyunsiparis_foreignkey_oyun : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.OyunSiparis", "OyunID");
            AddForeignKey("dbo.OyunSiparis", "OyunID", "dbo.Oyun", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OyunSiparis", "OyunID", "dbo.Oyun");
            DropIndex("dbo.OyunSiparis", new[] { "OyunID" });
        }
    }
}
