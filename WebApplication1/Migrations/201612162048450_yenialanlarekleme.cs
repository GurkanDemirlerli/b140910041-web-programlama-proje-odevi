namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class yenialanlarekleme : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Yorum", "KullaniciID");
            CreateIndex("dbo.Yorum", "OyunID");
            AddForeignKey("dbo.Yorum", "KullaniciID", "dbo.Kullanici", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Yorum", "OyunID", "dbo.Oyun", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yorum", "OyunID", "dbo.Oyun");
            DropForeignKey("dbo.Yorum", "KullaniciID", "dbo.Kullanici");
            DropIndex("dbo.Yorum", new[] { "OyunID" });
            DropIndex("dbo.Yorum", new[] { "KullaniciID" });
        }
    }
}
