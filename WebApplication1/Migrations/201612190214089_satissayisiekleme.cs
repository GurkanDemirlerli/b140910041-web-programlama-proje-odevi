namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class satissayisiekleme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Oyun", "Satilan", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Oyun", "Satilan");
        }
    }
}
