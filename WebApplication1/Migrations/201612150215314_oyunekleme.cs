namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oyunekleme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Oyun", "Puan", c => c.Int(nullable: false));
            AddColumn("dbo.Oyun", "Video", c => c.String());
            AddColumn("dbo.Oyun", "MinSistem", c => c.String());
            AddColumn("dbo.Oyun", "OnerilenSistem", c => c.String());
            AddColumn("dbo.Oyun", "Tanitim", c => c.String());
            AddColumn("dbo.Oyun", "Platform", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Oyun", "Platform");
            DropColumn("dbo.Oyun", "Tanitim");
            DropColumn("dbo.Oyun", "OnerilenSistem");
            DropColumn("dbo.Oyun", "MinSistem");
            DropColumn("dbo.Oyun", "Video");
            DropColumn("dbo.Oyun", "Puan");
        }
    }
}
