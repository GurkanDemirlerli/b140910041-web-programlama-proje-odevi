namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class gg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CdKey", "Oyun_Id", c => c.Int());
            CreateIndex("dbo.CdKey", "Oyun_Id");
            AddForeignKey("dbo.CdKey", "Oyun_Id", "dbo.Oyun", "Id");
            DropColumn("dbo.CdKey", "OyunID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CdKey", "OyunID", c => c.Int(nullable: false));
            DropForeignKey("dbo.CdKey", "Oyun_Id", "dbo.Oyun");
            DropIndex("dbo.CdKey", new[] { "Oyun_Id" });
            DropColumn("dbo.CdKey", "Oyun_Id");
        }
    }
}
