namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KullaniciDateTimeVeYorum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Yorum",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        OyunID = c.Int(nullable: false),
                        KullaniciID = c.Int(nullable: false),
                        YorumTuru = c.String(),
                        Icerik = c.String(),
                        Tarih = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Kullanici", "KayitTarihi", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Kullanici", "KayitTarihi");
            DropTable("dbo.Yorum");
        }
    }
}
