namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kullaniciprofilresmiekleme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kullanici", "ProfilResmi", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Kullanici", "ProfilResmi");
        }
    }
}
