namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ekleme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kullanici", "Email", c => c.String());
            AddColumn("dbo.Kullanici", "OnaylaParola", c => c.String());
            AlterColumn("dbo.Kullanici", "Adi", c => c.String(nullable: false));
            AlterColumn("dbo.Kullanici", "Soyadi", c => c.String(nullable: false));
            AlterColumn("dbo.Kullanici", "KullaniciAdi", c => c.String(nullable: false));
            AlterColumn("dbo.Kullanici", "Parola", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Kullanici", "Parola", c => c.String());
            AlterColumn("dbo.Kullanici", "KullaniciAdi", c => c.String());
            AlterColumn("dbo.Kullanici", "Soyadi", c => c.String());
            AlterColumn("dbo.Kullanici", "Adi", c => c.String());
            DropColumn("dbo.Kullanici", "OnaylaParola");
            DropColumn("dbo.Kullanici", "Email");
        }
    }
}
