﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Veri;

namespace WebApplication1.Controllers
{
    public class KullaniciController : Controller
    {
        //
        // GET: /Kullanici/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Siparislerim()
        {
            return View();
        }
        public ActionResult OyunSiparislerim()
        {
           int kid = Convert.ToInt32(Session["KullaniciID"]);
            SarayContext db = new SarayContext();
            List<OyunSiparis> siparislerim = db.OyunSiparis.Where(x => x.KullaniciID == kid).ToList();
            return View(siparislerim);
        }
        public ActionResult OnlineOyunSiparislerim()
        {
            return View();
        }
        public ActionResult BakiyeYukle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult BakiyeYukle(double bky)
        {
            int kid=Convert.ToInt32(Session["KullaniciID"]);
            SarayContext db = new SarayContext();
            Kullanici Kullanici = db.Kullanicilar.Where(x => x.ID == kid).Single();
            Kullanici.Bakiye += bky;
            var _kullanici = db.Kullanicilar.Single(a => a.ID == kid);
            db.Entry(_kullanici).CurrentValues.SetValues(Kullanici);
            db.SaveChanges();
            Session["Bakiye"] = _kullanici.Bakiye;
            return View();
        }
        public ActionResult Duzenle()
        {
            int kid = Convert.ToInt32(Session["KullaniciID"]);
            SarayContext db=new SarayContext();
            Kullanici Kullanici = db.Kullanicilar.Where(x => x.ID == kid).Single();
            return View(Kullanici);
        }
        [HttpPost]
        public ActionResult Duzenle(Kullanici klnc)
        {
            if (klnc.Parola == klnc.OnaylaParola)
            {
                int kid = Convert.ToInt32(Session["KullaniciID"]);
                using (SarayContext db = new SarayContext())
                {
                    Kullanici Kullanici = db.Kullanicilar.Where(x => x.ID == kid).Single();
                    var _kullanici = db.Kullanicilar.Single(a => a.ID == kid);
                    Kullanici.Adi = klnc.Adi;
                    Kullanici.Email = klnc.Email;
                    Kullanici.OnaylaParola = klnc.OnaylaParola;
                    Kullanici.Parola = klnc.Parola;
                    Kullanici.Soyadi = klnc.Soyadi;
                    db.Entry(_kullanici).CurrentValues.SetValues(Kullanici);
                    db.SaveChanges();
                    return View(Kullanici);
                }
            }
            else
            {
                int kid = Convert.ToInt32(Session["KullaniciID"]);
                SarayContext db = new SarayContext();
                Kullanici Kullanici = db.Kullanicilar.Where(x => x.ID == kid).Single();
                return View(Kullanici);
            }
        }
        public ActionResult ProfilResmiGuncelle(HttpPostedFileBase uploadfile)
        {
            int kid = Convert.ToInt32(Session["KullaniciID"]);
            SarayContext db = new SarayContext();
            Kullanici Kullanici = db.Kullanicilar.Where(x => x.ID == kid).Single();
            var _kullanici = db.Kullanicilar.Single(a => a.ID == kid);
            string Klasor, filePath, pathForDb;
            if (uploadfile.ContentLength > 0)
            {
                Klasor = Path.Combine(Server.MapPath("~/img/profil"));
                if (!Directory.Exists(Klasor))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Klasor);
                }
                filePath = Path.Combine(Klasor, (Kullanici.Adi + Path.GetExtension(uploadfile.FileName)));
                uploadfile.SaveAs(filePath);
                pathForDb = Kullanici.Adi + Path.GetExtension(uploadfile.FileName);
                Kullanici.ProfilResmi = pathForDb;
                db.Entry(_kullanici).CurrentValues.SetValues(Kullanici);
                db.SaveChanges();
            }
            return RedirectToAction("Duzenle", "Kullanici");
        }
    }
}