﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Veri;

namespace WebApplication1.Controllers
{
    public class DenemeController : Controller
    {
        private SarayContext db = new SarayContext();

        // GET: /Deneme/
        public ActionResult Index()
        {
            return View(db.Oyunlar.ToList());
        }

        // GET: /Deneme/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyunlar.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // GET: /Deneme/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Deneme/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Adi,Fiyat,Kategori")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Oyunlar.Add(oyun);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oyun);
        }

        // GET: /Deneme/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyunlar.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: /Deneme/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Adi,Fiyat,Kategori")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oyun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oyun);
        }

        // GET: /Deneme/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyunlar.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: /Deneme/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oyun oyun = db.Oyunlar.Find(id);
            db.Oyunlar.Remove(oyun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
