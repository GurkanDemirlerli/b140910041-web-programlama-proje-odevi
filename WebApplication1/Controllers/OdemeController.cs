﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Veri;
using WebApplication1.Models;
using System.Data.Entity.Validation;
using System.Data;
using System.Data.Entity;

namespace WebApplication1.Controllers
{
    public class OdemeController : Controller
    {
        //
        // GET: /Odeme/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Oyun(int oyunId)
        {
            SarayContext db = new SarayContext();
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == oyunId).SingleOrDefault();
            ViewData["id"] = oyunId.ToString();
            return View(Oyun);
        }
        [HttpPost]
        public ActionResult Oyun(string oyunId,string kullaniciId)
        {
            int oid = Convert.ToInt32(oyunId);
            int kid = Convert.ToInt32(kullaniciId);
            using (SarayContext db=new SarayContext())
            {
                Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).SingleOrDefault();
                Kullanici Kullanici = db.Kullanicilar.Where(i => i.ID == kid).SingleOrDefault();
                var _oyun = db.Oyunlar.Single(a => a.Id == oid);
                var _kullanici = db.Kullanicilar.Single(a => a.ID == kid);
                Oyun.StokDurumu = db.CdKey.Where(x => x.Durum == false).Count()-1;
                if (_kullanici.Bakiye>=_oyun.Fiyat)
                {
                Kullanici.Bakiye = Convert.ToDouble( Kullanici.Bakiye) -Convert.ToDouble(Oyun.Fiyat);
                Oyun.StokDurumu = Convert.ToInt32(Oyun.StokDurumu) - 1;
                db.Entry(_oyun).CurrentValues.SetValues(Oyun);
                db.Entry(_kullanici).CurrentValues.SetValues(Kullanici);
                db.SaveChanges();
                CdKey _cdkey = _oyun.CdKey.Where(x => x.Durum == false).First();
                Kullanici.OyunSiparis.Add(new OyunSiparis { 
                    CdKey=_cdkey,
                    Oyun=_oyun,
                    OyunID=_oyun.Id,
                    CdKeyID=_cdkey.CdKeyID,
                    Tarih=DateTime.Now
                });
                CdKey cdkey = _cdkey;
                cdkey.Durum = true;//true satılmış demek.
                db.Entry(_cdkey).CurrentValues.SetValues(cdkey);
                db.SaveChanges();

                Session["Bakiye"] = _kullanici.Bakiye.ToString();
                }
                else
                {
                    return RedirectToAction("Basarisiz");
                }
          
            return RedirectToAction("Basarili");
            }
        }
        public ActionResult Basarili()
        {
            return View();
        }
        public ActionResult Basarisiz()
        {
            return View();
        }
        public ActionResult ParaYukle()
        {
            return View();
        }
	}
}