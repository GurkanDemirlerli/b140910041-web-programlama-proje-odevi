﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Veri;
    
namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult OyunEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OyunEkle(Oyun oyn)
        {
            using (SarayContext db = new SarayContext())
            {
                oyn.Tarih = DateTime.Now;
                oyn.StokDurumu = 0;
                oyn.Satilan = 0;
                oyn.cover = "/img/cover/infc.jpg";
                oyn.scr1 = "/img/scr/infs.jpg";
                oyn.scr2 = "/img/scr/infs.jpg";
                oyn.scr3 = "/img/scr/infs.jpg";
                oyn.scr4 = "/img/scr/infs.jpg";
                oyn.scr5 = "/img/scr/infs.jpg";
                oyn.scr6 = "/img/scr/infs.jpg";
                oyn.banner = "/img/banner/infb.jpg";
                db.Oyunlar.Add(oyn);
                db.SaveChanges();

                return RedirectToAction("OyunResimEkle", new {oyunId=db.Oyunlar.OrderByDescending(x=>x.Tarih).First().Id.ToString() });
            }
        }
        public ActionResult OyunResimEkle(string oyunId)
        {
            int oid = Convert.ToInt32(oyunId);
            using   (SarayContext db = new SarayContext())
            { 
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).Single();
            
            return View(Oyun);
            }
        }
        [HttpPost]
        public ActionResult OyunResimEkle(string oyunId,string property,HttpPostedFileBase uploadfile)
        {
            string Klasor, filePath, pathForDb;
            int oid = Convert.ToInt32(oyunId);
            SarayContext db = new SarayContext();
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).Single();
            var _oyun = db.Oyunlar.Single(a => a.Id == oid);
            if (uploadfile.ContentLength > 0)
            {
                switch (property)
                {
                    case "cover":
                        Klasor = Path.Combine(Server.MapPath("~/img/cover"));
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, (Oyun.Adi + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb ="/img/cover/"+Oyun.Adi+Path.GetExtension(uploadfile.FileName);
                        Oyun.cover = pathForDb; 
                        break;
                    case "banner":
                        Klasor = Path.Combine(Server.MapPath("~/img/banner"));
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, (Oyun.Adi + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/banner/" + Oyun.Adi + Path.GetExtension(uploadfile.FileName);
                        Oyun.banner = pathForDb;
                        break;
                    case "scrs1":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs1" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi + "/" + "scrs1" + Path.GetExtension(uploadfile.FileName);
                        Oyun.scr1 = pathForDb;
                        break;
                    case "scrs2":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs2" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi+"/" +"scrs2"+ Path.GetExtension(uploadfile.FileName);
                        Oyun.scr2 = pathForDb;
                        break;
                    case "scrs3":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs3" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi + "/" + "scrs3" + Path.GetExtension(uploadfile.FileName);
                        Oyun.scr3 = pathForDb;
                        break;
                    case "scrs4":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs4" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi + "/" + "scrs4" + Path.GetExtension(uploadfile.FileName);
                        Oyun.scr4 = pathForDb;
                        break;
                    case "scrs5":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs5" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi + "/" + "scrs5" + Path.GetExtension(uploadfile.FileName);
                        Oyun.scr5 = pathForDb;
                        break;
                    case "scrs6":
                        Klasor = Path.Combine(Server.MapPath("~/img/scr"), Oyun.Adi);
                        if (!Directory.Exists(Klasor))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Klasor);
                        }
                        filePath = Path.Combine(Klasor, ("scrs6" + Path.GetExtension(uploadfile.FileName)));
                        uploadfile.SaveAs(filePath);
                        pathForDb = "/img/scr/" + Oyun.Adi + "/" + "scrs6" + Path.GetExtension(uploadfile.FileName);
                        Oyun.scr6 = pathForDb;
                        break;
                    default:
                        break;
                }
                db.Entry(_oyun).CurrentValues.SetValues(Oyun);
                db.SaveChanges();
               
            }
            return View(Oyun);
        }

        public ActionResult OyunAra()
        {
            return View();
        }

        [HttpGet]
        public ActionResult OyunAra(string aranan)
        {
            SarayContext db = new SarayContext();
            List<Oyun> Oyun = db.Oyunlar.Where(x => x.Adi.Contains(aranan)).ToList();
            return View(Oyun);
        }
        public ActionResult OyunCdKeyEkle(string oyunId)
        {
            int oid = Convert.ToInt32(oyunId);
            ViewData["id"] = oyunId.ToString();
            using (SarayContext db = new SarayContext())
            {
                Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).Single();

                return View(Oyun);
            }
        }
        [HttpPost]
        public ActionResult OyunCdKeyEkle(string oyunId, string key)
        {
            int oid = Convert.ToInt32(oyunId);
            ViewData["id"] = oyunId.ToString();
            using (SarayContext db = new SarayContext())
            {
                Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).Single();
                var _oyun = db.Oyunlar.Single(a => a.Id == oid);
                Oyun.CdKey.Add(new CdKey { Durum = false, Key = key, Tarih = DateTime.Now });
                Oyun.StokDurumu += 1;
                db.Entry(_oyun).CurrentValues.SetValues(Oyun);
                db.SaveChanges();
                return View(Oyun);
            }
        }
        public ActionResult IletisimMesajlari()
        {
            SarayContext db = new SarayContext();
            List<Iletisim> msjlr = db.IletisimMesajlari.OrderByDescending(x=>x.Tarih).ToList();

            return View(msjlr);
        }

        public ActionResult OyunBilgileriDuzenle(string oyunId)
        {
            int oid = Convert.ToInt32(oyunId);
            SarayContext db = new SarayContext();
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == oid).Single();
            return View(Oyun);
        }
        [HttpPost]
        public ActionResult OyunBilgileriDuzenle(Oyun oyn)
        {
           using ( SarayContext db = new SarayContext())
           {

           
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == oyn.Id).Single();
            Oyun.Adi = oyn.Adi;
            Oyun.Fiyat = oyn.Fiyat;
            Oyun.Kategori = oyn.Kategori;
            Oyun.MinSistem = oyn.MinSistem;
            Oyun.OnerilenSistem = oyn.OnerilenSistem;
            Oyun.Platform = oyn.Platform;
            Oyun.Puan = oyn.Puan;
            Oyun.Video = oyn.Video;
            var _oyun = db.Oyunlar.Single(a => a.Id == oyn.Id);
            db.Entry(_oyun).CurrentValues.SetValues(Oyun);
            db.SaveChanges();
         
            return View(Oyun);
           }
        }

    }
}