﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Veri;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class OyunController : Controller
    {
        SarayContext db = new SarayContext();
        //
        // GET: /Oyun/
        public ActionResult Index()
        {
            SarayContext db = new SarayContext();
            List<Oyun> Oyunlar = db.Oyunlar.OrderByDescending(x => x.Tarih).ToList();
            return View(Oyunlar);
        }
        public ActionResult Oyunlar(string kategori)
        {
            ViewData["kategori"] = kategori;
            using (SarayContext db = new SarayContext())
            {

                List<Oyun> Oyunlar = db.Oyunlar.Where(x => x.Kategori.Contains(kategori)).ToList();
                return View(Oyunlar);
            }
        }
        public ActionResult id(int id)
        {
            List<object> myModel = new List<object>();
            myModel.Add(db.Oyunlar.OrderBy(x => x.Tarih).ToList());
            myModel.Add(db.Kullanicilar.ToList());
            myModel.Add(db.Yorumlar.OrderBy(x=>x.Tarih).ToList());
            ViewData["id"] = id.ToString();
            return View(myModel);
        }

        public ActionResult Oyun(int id)
        {
            SarayContext db = new SarayContext();
            Oyun Oyun = db.Oyunlar.Where(x => x.Id == id).SingleOrDefault();
            ViewData["id"] = id.ToString();
            return View(Oyun);
        }
        [HttpPost]
        public ActionResult Oyun(int id, int KullaniciID, string YorumTuru, string Icerik)
        {
            using (SarayContext db = new SarayContext())
            { 
                Yorum _yrm = new Yorum();
                _yrm.OyunID = id;
                _yrm.KullaniciID = KullaniciID;
                _yrm.YorumTuru = YorumTuru;
                _yrm.Icerik = Icerik;
                _yrm.Tarih = DateTime.Now;
                db.Yorumlar.Add(_yrm);
                db.SaveChanges();
            }
            ModelState.Clear();
                SarayContext dba = new SarayContext();
                Oyun Oyun = dba.Oyunlar.Where(x => x.Id == id).SingleOrDefault();
                ViewData["id"] = id.ToString();
                return View(Oyun);

        }
        [HttpPost]
        public ActionResult id(int OyunID, int KullaniciID, string YorumTuru, string Icerik)
        {
            using (SarayContext db = new SarayContext())
            {
                Yorum _yrm = new Yorum();
                _yrm.OyunID = OyunID;
                _yrm.KullaniciID = KullaniciID;
                _yrm.YorumTuru = YorumTuru;
                _yrm.Icerik = Icerik;
                _yrm.Tarih = DateTime.Now;
                db.Yorumlar.Add(_yrm);
                db.SaveChanges();
                List<object> myModel = new List<object>();
                myModel.Add(db.Oyunlar.ToList());
                myModel.Add(db.Kullanicilar.ToList());
                myModel.Add(db.Yorumlar.ToList());
                ViewData["id"] = OyunID.ToString();
                return View(myModel);
            }
        }
        public ActionResult TabYeniCikanlar()
        {
            int i = 0;
            SarayContext db = new SarayContext();
            List<Oyun> _Oyun = new List<Oyun>();
            List<Oyun> Oyun = new List<Oyun>();
            _Oyun=(db.Oyunlar.OrderByDescending(x => x.Tarih).ToList());
            foreach (var item in _Oyun)
            {
                Oyun.Add(item);
                i++;
                if (i>=4)
                {
                    break;
                }
            }
            return View(Oyun);
        }

        public ActionResult TabCokSatanlar()
        {
            int i = 0;
            SarayContext db = new SarayContext();
            List<Oyun> _Oyun = new List<Oyun>();
            List<Oyun> Oyun = new List<Oyun>();
            _Oyun = (db.Oyunlar.OrderByDescending(x => x.Satilan).ToList());
            foreach (var item in _Oyun)
            {
                Oyun.Add(item);
                i++;
                if (i >= 4)
                {
                    break;
                }
            }
            return View(Oyun);
        }


        public ActionResult TabBenzerler(Oyun oyn)
        {
            int adUz, katUz;
            SarayContext db = new SarayContext();
            List<Oyun> Oyun = new List<Oyun>();
            List<Oyun> ad=new List<Oyun>();
            List<Oyun> kategori = new List<Oyun>();
            List<Oyun> genel = new List<Oyun>();
            int i = 0;
            char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
            string[] oyunAdiKelime = oyn.Adi.Split(delimiterChars);
            string[] kategoriKelime = oyn.Kategori.Split(delimiterChars);
            foreach (string s in oyunAdiKelime)
            {
                ad= db.Oyunlar.Where(x => x.Adi.Contains(s)).ToList();
                ad.Remove(db.Oyunlar.Where(x => x.Id == oyn.Id).Single());
            }
            adUz = ad.Count();
            foreach (string s in kategoriKelime)
            {
                kategori = db.Oyunlar.Where(x => x.Adi.Contains(s)).ToList();
                kategori.Remove(db.Oyunlar.Where(x => x.Id == oyn.Id).Single());
            }
            katUz = kategori.Count();
            genel = db.Oyunlar.ToList();
            genel.Remove(db.Oyunlar.Where(x => x.Id == oyn.Id).Single());
            foreach (var item in ad)
            {
                Oyun.Add(item);
                i++;
                if (i >= 4)
                {
                    break;
                }
            }
            i = 0;
            if (adUz<4)
            {
                foreach (var item in kategori)
                {
                    Oyun.Add(item);
                    i++;
                    if (i >= (4-adUz))
                    {
                        break;
                    }
                }
                i = 0;
                if (adUz+katUz<4)
                {
                    foreach (var item in genel)
                    {
                        Oyun.Add(item);
                        i++;
                        if (i >= (4 - adUz+katUz))
                        {
                            break;
                        }
                    }
                }
            }
            return View(Oyun);
        }

    }
}