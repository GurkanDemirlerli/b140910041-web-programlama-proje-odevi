﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Veri;
namespace WebApplication1.Controllers
{
    public class HesapController : Controller
    {
        //
        // GET: /Hesap/
        public ActionResult Index()
        {
            using (SarayContext db=new SarayContext())
            {
                return View(db.Kullanicilar.ToList());
            }
        }
        public ActionResult UyeOl()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UyeOl(Kullanici Hesap)
        {
            if (ModelState.IsValid)
            {
                using(SarayContext db=new SarayContext())
                {
                    Hesap.KayitTarihi = DateTime.Now;
                    Hesap.Admin = false;
                    Hesap.ProfilResmi = "varsayilan.png";
                    Hesap.Bakiye = 0;
                    db.Kullanicilar.Add(Hesap);
                    db.SaveChanges();
                }
            ModelState.Clear();
            ViewBag.Message = Hesap.Adi + " " + Hesap.Soyadi + "Başarıyla Kayıt Oldu";
            }
            return View();
        }
        //Login
        public ActionResult Giris()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Giris(Kullanici kullanici)
        {
            using(SarayContext db=new SarayContext())
            {
                var usr = db.Kullanicilar.Where(i => i.KullaniciAdi == kullanici.KullaniciAdi && i.Parola == kullanici.Parola).FirstOrDefault();
                if(usr!=null)
                {
                    Session["KullaniciID"] = usr.ID.ToString();
                    Session["KullaniciAdi"] = usr.KullaniciAdi.ToString();
                    Session["ProfilResmi"] = usr.ProfilResmi.ToString();
                    Session["Bakiye"] = usr.Bakiye.ToString();
                    if (usr.Admin == true)
                        Session["KullaniciAdmin"] = "1";
                    else
                        Session["KullaniciAdmin"] = "0";
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    ModelState.AddModelError("", "Kullanıcı Adı veya Parola Yanlış.");
                }
            }
            return View();
        }
        public ActionResult LoggedIn()
        {
            if (Session["KullaniciID"]!=null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Giris");
            }
        }
        public ActionResult Cikis()
        {
            Session.Abandon();
            return RedirectToAction("Giris");
        }
	}
}