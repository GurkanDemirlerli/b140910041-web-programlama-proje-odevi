﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Veri;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        SarayContext islemler = new SarayContext();
        public ActionResult Index()
        {
            List<Oyun> Oyunlar = islemler.Oyunlar.OrderByDescending(x=>x.Tarih).ToList();
            return View(Oyunlar);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Oyun()
        {

            return View();
        }
        [HttpGet]
        public ActionResult Ara(string aranan)
        {
            SarayContext db = new SarayContext();
            List<Oyun> Oyun = db.Oyunlar.Where(x => x.Adi.Contains(aranan)).ToList();
            return View(Oyun);
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Iletisim(Iletisim iltsm)
        {
            if (ModelState.IsValid)
            {
                using (SarayContext db = new SarayContext())
                {
                    iltsm.Tarih = DateTime.Now;
                    db.IletisimMesajlari.Add(iltsm);
                    db.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message =  "Başarıyla Mesaj Gönderdiniz.";
            }
            return View();
        }
    }
}