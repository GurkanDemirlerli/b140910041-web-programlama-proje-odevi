﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Veri
{
    public class BaslangicData:System.Data.Entity.DropCreateDatabaseIfModelChanges<SarayContext>
    {
        protected override void Seed(SarayContext context)
        {
            var Oyunlar = new List<Oyun>
            {
                new Oyun{Adi="Battlefield 1",Fiyat=256.10,Kategori="Aksiyon",Tarih=DateTime.Parse("15-12-2016")},
                new Oyun{Adi="Call Of Duty Black Ops III",Fiyat=56.10,Kategori="Aksiyon",Tarih=DateTime.Parse("14-12-2016")}

            };
            Oyunlar.ForEach(s => context.Oyunlar.Add(s));
            context.SaveChanges();
        }
    }
}