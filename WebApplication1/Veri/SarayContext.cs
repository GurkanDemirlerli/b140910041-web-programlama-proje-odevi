﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebApplication1.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WebApplication1.Veri
{
    public class SarayContext : DbContext
    {
        public SarayContext():base("SarayGameVeriTabani")
        {

        }
        public DbSet<Oyun> Oyunlar { get; set; }
        public DbSet<Kullanici> Kullanicilar { get; set; }
        public DbSet<Yorum> Yorumlar { get; set; }
        public DbSet<OyunSiparis> OyunSiparis { get; set; }
        public DbSet<CdKey> CdKey { get; set; }
        public DbSet<Iletisim> IletisimMesajlari { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelbuilder)
        {
            modelbuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

       
    }
}